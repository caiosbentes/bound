#!/bin/bash
#
# glpi.sh
#
# Autor     : Caio Bentes <caio.bentes@solustecnologia.com.br>
#
#  -------------------------------------------------------------
#   Este programa instala o dokuwiki  em um servidor Debian 9.X
#

#Muda o mirror do SO
echo "deb http://ftp.br.debian.org/debian/ stretch main" > /etc/apt/sources.list
echo "deb-src http://ftp.br.debian.org/debian/ stretch main" >> /etc/apt/sources.list

#Atualiza lista de pacotes e atualiza os pacotes
apt-get update && apt-get upgrade -y
apt-get install nginx
service nginx start
apt-get install php7.0-fpm php7.0-gd
sed -i '/;cgi.fix_pathinfo=1/ s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php/7.0/fpm/php.ini
cp /etc/nginx/sites-available/default /root/default
 service nginx restart

