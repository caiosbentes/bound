#!/bin/bash
#
# glpi.sh
#
# Autor     : Caio Bentes <caio.bentes@solustecnologia.com.br>
#
#  -------------------------------------------------------------
#   Este programa instala o glpi 9.3.0 em um servidor Debian 9.X
#

#Muda o mirror do SO
echo "deb http://ftp.br.debian.org/debian/ stretch main" > /etc/apt/sources.list
echo "deb-src http://ftp.br.debian.org/debian/ stretch main" >> /etc/apt/sources.list

#Atualiza lista de pacotes e atualiza os pacotes
apt-get update && apt-get upgrade -y
#Instala pacotes nescessários
apt-get install -y ca-certificates apache2 libapache2-mod-php7.0 php7.0-fpm php7.0-apcu php7.0-xmlrpc php7.0-xmlrpc php7.0-mbstring php7.0-xml php7.0-xmlrpc php7.0-dev php7.0-gd
apt-get install -y php7.0-cli php7.0-curl php7.0 php7.0-gd php7.0-imap php7.0-ldap php7.0-mysql php-soap php7.0-mbstring php7.0-xml php7.0-xmlrpc php-cas mariadb-server git

#Baixa o glpi para o diretório tmp, descompacta e move para /var/www/html
cd /tmp
wget https://github.com/glpi-project/glpi/releases/download/9.3.0/glpi-9.3.tgz
tar zxvf glpi-9.3.tgz
mkdir /var/www/html/glpi
mv glpi/* /var/www/html/glpi
#Modifica o dono dos arquivos do glpi e muda suas permições
chown -R www-data:www-data /var/www/html/glpi/
chmod -Rf 775 /var/www/html/glpi/

#a2enconf glpi.conf

sed -i 's,/var/www/html,/var/www/html/glpi,g' /etc/apache2/sites-enabled/000-default.conf

#cria seta a pasta do glpi para o apache resolver
cat <<EOF >> /etc/apache2/apache2.conf
<Directory /var/www/html/glpi/>
AllowOverride All
</Directory>
<Directory /var/www/html/glpi/config> 
Options -Indexes
</Directory>
<Directory /var/www/html/glpi/files> 
Options -Indexes
</Directory>
EOF



#restarta o servidor http apache2
service apache2 restart

#configura o banco de dados 
read -sp "Qual a senha do root do banco de dados? " ROOTDBPWD
echo
debconf-set-selections <<< "mariadb-server-10.0 mariadb-server/root_password password $ROOTDBPWD"
debconf-set-selections <<< "mariadb-server-10.0 mariadb-server/root_password_again password $ROOTDBPWD"
debconf-set-selections <<< "mariadb-server-10.0 mariadb-server/oneway_migration boolean true"
echo "create database glpi;" | mysql -uroot
echo "create user glpi@localhost identified by 'Solus@1010';" | mysql -uroot
echo "grant all on glpi.* to glpi identified by 'Solus@1010';" | mysql -uroot


# AQUI VOCE INSTALA O GLPI VIA http://my-ip/installation.php


cd /var/www/html/glpi/plugins
#Instala o plugin modifications
wget https://ufpr.dl.sourceforge.net/project/glpithemes/9.3/Plugin_Modifications_1.2.1_GLPI_9.3.zip
apt install zip unzip
unzip Plugin_Modifications_1.2.1_GLPI_9.3.zip
rm Plugin_Modifications_1.2.1_GLPI_9.3.zip
#Instala o plugin dashboard
wget https://ufpr.dl.sourceforge.net/project/glpidashboard/GLPI_9.2.x/GLPI-dashboard_plugin-0.9.3.tar.gz
tar zxvf GLPI-dashboard_plugin-0.9.3.tar.gz
rm GLPI-dashboard_plugin-0.9.3.tar.gz

wget https://codeload.github.com/pluginsGLPI/datainjection/tar.gz/2.6.0
tar zxvf 2.6.0
mv datainjection-2.6.0 datainjection
rm 2.6.0

wget http://www.glpibrasil.com.br/download/glpi-importacao-automatica-do-catalogo-de-servicos-datainjection/?wpdmdl=4033


#Download e substituição da imagem acima dos campos de login
wget https://i.imgur.com/P6Iq640.png
mv P6Iq640.png /var/www/html/glpi/pics/logo.png

wget https://imgur.com/ySxfSih.png
mv ySxfSih.png /var/www/html/glpi/pics/logo_big.png

wget https://imgur.com/KWtQUwK.jpg
mv KWtQUwK.jpg /var/www/html/glpi/pics/bg/back.jpg

wget https://imgur.com/tF62bgs.png
mv tF62bgs.png /var/www/html/glpi/pics/fd_logo.png

wget https://www.iconfinder.com/icons/1055084/download/png/32
mv 32 /var/www/html/glpi/pics/favicon.ico


sed -i '/<head><title>/ s/GLPI/CENTRAL/g' /var/www/html/glpi/inc/html.class.php
sed -i '/<title>/ s/GLPI/CENTRAL/g' /var/www/html/glpi/index.php
sed -i '/<title>/ s/Authentication/Autenticação/g' /var/www/html/glpi/index.php


rm /var/www/html/glpi/install/install.php


php /var/www/html/glpi/scripts/innodb_migration.php



#echo "drop database glpi;" | mysql -uroot
#echo "drop user glpi@localhost" | mysql -uroot
