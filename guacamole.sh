#!/bin/bash
#
# glpi.sh
#
# Autor     : Caio Bentes <caio.bentes@solustecnologia.com.br>
#
#  -------------------------------------------------------------
#   Este programa instala o apache guacamole em um servidor Debian 9.X
#

#Muda o mirror do SO
echo "deb http://ftp.br.debian.org/debian/ stretch main" > /etc/apt/sources.list
echo "deb-src http://ftp.br.debian.org/debian/ stretch main" >> /etc/apt/sources.list

#Atualiza lista de pacotes e atualiza os pacotes
apt-get update && apt-get upgrade -y