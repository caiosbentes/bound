# !/bin/bash
# Interfaces Nutriama

systemctl stop NetworkManager
systemctl disable NetworkManager

cd /etc/sysconfig/network-scripts/
sed -i '/ONBOOT/ s/no/yes/g' ifcfg-p1p1
sed -i '/ONBOOT/ s/no/yes/g' ifcfg-p1p2
sed -i '/ONBOOT/ s/no/yes/g' ifcfg-p2p1
sed -i '/ONBOOT/ s/no/yes/g' ifcfg-p2p2
sed -i '/ONBOOT/ s/no/yes/g' ifcfg-p3p1
sed -i '/ONBOOT/ s/no/yes/g' ifcfg-p3p2
sed -i '/ONBOOT/ s/no/yes/g' ifcfg-p4p1
sed -i '/ONBOOT/ s/no/yes/g' ifcfg-p4p2


cp ifcfg-p1p1 ifcfg-p1p1.bkp21062018
cp ifcfg-p1p2 ifcfg-p1p2.bkp21062018
cp ifcfg-p2p1 ifcfg-p2p1.bkp21062018
cp ifcfg-p2p2 ifcfg-p2p2.bkp21062018
cp ifcfg-p3p1 ifcfg-p3p1.bkp21062018
cp ifcfg-p3p2 ifcfg-p3p2.bkp21062018
cp ifcfg-p4p1 ifcfg-p4p1.bkp21062018
cp ifcfg-p4p2 ifcfg-p4p2.bkp21062018


mv ifcfg-p1p2 ifcfg-bond0-p1p2
mv ifcfg-p2p2 ifcfg-bond0-p2p2
mv ifcfg-p3p1 ifcfg-bond1-p3p1
mv ifcfg-p4p1 ifcfg-bond1-p4p1
mv ifcfg-p3p2 ifcfg-bond2-p3p2
mv ifcfg-p4p2 ifcfg-bond2-p4p2

#Configuração da interface p1p2
cat <<EOF > ifcfg-bond0-p1p2
NM_CONTROLLED=no
MASTER=bond0
SLAVE=yes
NAME=bond0-p1p2
UUID=fb4c00ee-b723-4b88-886d-c751a0f75def
DEVICE=p1p2
ONBOOT=yes
EOF

#Configuração da interface p2p2
cat <<EOF > ifcfg-bond0-p2p2
BOOTPROTO=none
NM_CONTROLLED=no
MASTER=bond0
SLAVE=yes
NAME=bond0-p2p2
UUID=d1bb0930-ffd7-4adc-ade3-dbcae1c3a46e
DEVICE=p2p2
ONBOOT=yes
EOF

#Configuração do bonding 0
touch ifcfg-bond0

UUID=$(uuidgen)
cat <<EOF > ifcfg-bond0
DEVICE=bond0
MASTER=yes
BOOTPROTO=none
ONBOOT=yes
USERCTL=no
NAME=bond0
EOF
echo "UUID=${UUID}" >> ifcfg-bond0
cat <<EOF >> ifcfg-bond0
IPADDR=192.168.100.200
NETMASK=255.255.255.0
BROADCAST=192.168.100.255
NETWORK=192.168.100.0
GATEWAY=192.168.100.51
DNS1=192.168.100.216
DNS2=192.168.100.223
DOMAIN=nutriama.local
EOF

#Configuração da interface p3p1
cat <<EOF > ifcfg-bond1-p3p1
BOOTPROTO=none
NM_CONTROLLED=no
MASTER=bond1
SLAVE=yes
NAME=bond1-p3p1
UUID=75e8e648-2b13-4582-bf20-5c7ef9947007
DEVICE=p3p1
ONBOOT=yes
EOF

#Configuração da interface p4p1
cat <<EOF > ifcfg-bond1-p4p1
BOOTPROTO=none
NM_CONTROLLED=no
MASTER=bond1
SLAVE=yes
NAME=bond1-p4p1
UUID=35e59f8e-1ae9-4f67-a8c3-f91cd4fe1036
DEVICE=p4p1
ONBOOT=yes
EOF

#Configuração do bonding 1
touch ifcfg-bond1

UUID=$(uuidgen)
cat <<EOF > ifcfg-bond1
DEVICE=bond1
MASTER=yes
BOOTPROTO=none
ONBOOT=yes
USERCTL=no
DEFROUTE=yes
BONDING_OPTS="mode=4 miimon=100 lacp_rate=1"
NAME=bond0
EOF
echo "UUID=${UUID}" >> ifcfg-bond0
cat <<EOF >> ifcfg-bond0
DNS1=192.168.100.216
DNS2=192.168.100.223
DOMAIN=nutriama.local
IPADDR=192.168.100.202
PREFIX=24
GATEWAY=192.168.100.51
EOF

#Configuração da interface p3p2
cat <<EOF > ifcfg-bond2-p3p2
BOOTPROTO=none
NM_CONTROLLED=no
MASTER=bond2
SLAVE=yes
NAME=bond2-p3p2
UUID=cdc7cd9b-38ef-44a0-8c44-7bbca2e00d11
DEVICE=p3p2
ONBOOT=yes
EOF

#Configuração da interface p4p2
cat <<EOF > ifcfg-bond2-p4p2
BOOTPROTO=none
NM_CONTROLLED=no
MASTER=bond2
SLAVE=yes
NAME=bond2-p4p2
UUID=70f8dea4-a74e-4d71-892f-0340d60f52e3
DEVICE=p4p2
ONBOOT=yes
EOF

#Configuração do bonding 2
touch ifcfg-bond2

UUID=$(uuidgen)

cat <<EOF > ifcfg-bond2
DEVICE=bond2
MASTER=yes
BOOTPROTO=none
ONBOOT=yes
USERCTL=no
NAME=bond2
EOF
echo "UUID=${UUID}" >> ifcfg-bond2
cat <<EOF >> ifcfg-bond2
IPADDR=192.168.100.204
NETMASK=255.255.255.0
BROADCAST=192.168.100.255
NETWORK=192.168.100.0
GATEWAY=192.168.100.51
DNS1=192.168.100.216
DNS2=192.168.100.223
DOMAIN=nutriama.local
EOF

/etc/init.d/network restart