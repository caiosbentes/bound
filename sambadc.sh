#!/bin/bash
#
# sambdadc.sh
#
# Autor     : Caio Bentes <caio.bentes@solustecnologia.com.br>
#
#  -------------------------------------------------------------
#   Este programa vincula um DC Samba 4 (CentOS) a um AD Windows Server 2008
#

#Atualiza a lista de pacotes e atualiza pacotes
yum update -y && yum upgrade -y

#Seto as confirações do ip fixo do DC Samba
sed -i '/BOOTPROTO/ s/"dhcp"/"static"/g' /etc/sysconfig/network-scripts/ifcfg-enp0s3
sed -i '5i\IPADDR="192.168.10.143"' /etc/sysconfig/network-scripts/ifcfg-enp0s3
sed -i '6i\PREFIX="24"' /etc/sysconfig/network-scripts/ifcfg-enp0s3
sed -i '7i\NETMASK="255.255.255.0"' /etc/sysconfig/network-scripts/ifcfg-enp0s3
echo GATEWAY=\"192.168.10.1\" >> /etc/sysconfig/network-scripts/ifcfg-enp0s3
echo DNS1=\"192.168.10.187\" >> /etc/sysconfig/network-scripts/ifcfg-enp0s3
echo DOMAIN=\"ativacob.br\" >> /etc/sysconfig/network-scripts/ifcfg-enp0s3

#Restarto o serviço de Rede
systemctl restart network

#Seto hostname
hostnamectl set-hostname caio
systemctl restart systemd-hostnamed
#Faço meu host resolver pro meu ip
echo "192.168.10.143 caio.ativacob.br caio" >> /etc/hosts
#Instalar ferramentas de desenvolvimento
yum groupinstall 'Development Tools' -y
#Desabilita o firewall
systemctl stop firewalld
systemctl disable firewalld
#Desabilita o SELinux
sed -i '/SELINUX/ s/enforcing/disabled/g' /etc/selinux/config
setenforce 0
echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf

#Instala pacotes nescessários para compilação do samba
yum install -y perl gcc attr libacl-devel libblkid-devel gnutls-devel readline-devel \
python-devel gdb pkgconfig krb5-workstation zlib-devel setroubleshoot-server \
libaio-devel setroubleshoot-plugins policycoreutils-python libsemanage-python \
perl-ExtUtils-MakeMaker perl-Parse-Yapp perl-Test-Base popt-devel libxml2-devel \
libattr-devel keyutils-libs-devel cups-devel bind-utils libxslt docbook-style-xsl \
openldap-devel autoconf python-crypto pam-devel ntp wget vim

#Adiciono configurações samba no ntp 
cat <<EOF >> /etc/ntp.conf
# Configurações adicionais para o Samba 4
ntpsigndsocket /var/lib/samba/ntp_signd/
restrict default mssntp:
EOF
#Restarto o serviço do ntp
systemctl restart ntpd

#Muso o diretório para o /tmp
cd /tmp
#Faço download do samba em sua sersão 4.8.3
wget https://download.samba.org/pub/samba/stable/samba-4.8.2.tar.gz
#Descompacto os arquivos do samba
tar -zxvf samba-4.8.2.tar.gz
#Removo o Samba compactado
rm -f samba-4.8.2.tar.gz
#Entro na pasta do samba
cd samba-4.8.2
#Vamos gerar o Makefile e verificar se não há nenhuma dependência faltando e alguns ajustes extras
./configure --prefix /usr --enable-fhs --enable-cups --sysconfdir=/etc --localstatedir=/var --with-privatedir=/var/lib/samba/private --with-piddir=/var/run/samba --with-automount --datadir=/usr/share --with-lockdir=/var/run/samba --with-statedir=/var/lib/samba --with-cachedir=/var/cache/samba --with-systemd
#Vamos compilar o Samba 4
make
#Vamos a instalação dos arquivos, comandos, e bibliotecas nos seus respectivos diretórios
make install
#Vamos atualizar o cache de bibliotecas dinâmicas

 	
yum install bind bind-utils -y

ldconfig

cat <<EOF > /etc/krb5.conf
[libdefaults]
    dns_lookup_realm = false
    dns_lookup_kdc = true
    default_realm = ATIVACOB.BR
EOF


kinit ADMINISTRADOR

klist

samba-tool domain join --option="interfaces=lo enp0s3" --option="bind interfaces only=yes"

samba-tool domain join ativacob.br DC -U"ATIVACOB\administrador" --dns-backend=BIND9_DLZ --option="interfaces=lo enp0s3" --option="bind interfaces only=yes"

touch /lib/systemd/system/samba-ad-dc.service

cat <<EOF > /lib/systemd/system/samba-ad-dc.service
[Unit]
Description=Samba4 AD DC
After=network.target remote-fs.target nss-lookup.target 

[Service]
Type=forking
LimitNOFILE=16384
ExecStart=/usr/sbin/samba -D
ExecReload=/usr/bin/kill -HUP $MAINPID
PIDFile=/var/run/samba/samba.pid 

[Install]

WantedBy=multi-user.target:wq
EOF

systemctl daemon-reload 
systemctl start samba-ad-dc

#echo "include "/var/lib/samba/bind-dns/named.conf";" >> /etc/named.conf

#rm -f /var/lib/samba/private/secrets.ldb
#rm -f /var/lib/samba/private/secrets.tdb
