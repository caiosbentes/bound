# !/bin/bash
# Interfaces Formosa


cd /etc/sysconfig/network-scripts/


sed -i '/ONBOOT/ s/no/yes/g' ifcfg-em3
sed -i '/ONBOOT/ s/no/yes/g' ifcfg-em4
sed -i '/ONBOOT/ s/no/yes/g' ifcfg-p3p1
sed -i '/ONBOOT/ s/no/yes/g' ifcfg-p3p2

cp ifcfg-em1 ifcfg-em1.bkp21062018
cp ifcfg-em2 ifcfg-em2.bkp21062018
mv ifcfg-em1 ifcfg-team0-em1
mv ifcfg-em2 ifcfg-team0-em2
cp ifcfg-em3 ifcfg-team0-pub

cat <<EOF > ifcfg-team0-em1
NAME=team0-em1
EOF
sed -n '13p' ifcfg-em1.bkp21062018 >> ifcfg-team0-em1
cat <<EOF >> ifcfg-team0-em1
NAME=team0-em1
UUID=3d7e5cf3-518d-4f2d-95b9-23996a122a7c
DEVICE=em1
ONBOOT=yes
TEAM_MASTER=team0-pub
DEVICETYPE=TeamPort
EOF

cat <<EOF > ifcfg-team0-em2
NAME=team0-em2
EOF
sed -n '13p' ifcfg-em2.bkp21062018 >> ifcfg-team0-em2
cat <<EOF >> ifcfg-team0-em2
DEVICE=em2
ONBOOT=yes
TEAM_MASTER=team0-pub
DEVICETYPE=TeamPort
EOF


read -p "Qual seu IP (10.12.4.X)? " IPIP
UUID=$(uuidgen)
touch ifcfg-team0-pub

cat <<EOF > ifcfg-team0-pub
DEVICE=team0-pub
TEAM_CONFIG="{\"runner\": {\"name\":\"lacp\"}}"
BOOTPROTO=none
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=no
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=team0-pub
EOF

echo "UUID=${UUID}" >> ifcfg-team0-pub

cat <<EOF >> ifcfg-team0-pub
ONBOOT=yes
DEVICETYPE=Team
DNS1=10.12.0.20
DNS2=10.12.0.21
DOMAIN=grupoformosa.local
EOF


echo "IPADDR=${IPIP}" >> ifcfg-team0-pub

cat <<EOF >> ifcfg-team0-pub
PREFIX=21
GATEWAY=10.12.1.1
IPV6_PEERDNS=yes
IPV6_PEERROUTES=yes
EOF

/etc/init.d/network restart